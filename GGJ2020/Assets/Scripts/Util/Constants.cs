﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    public static readonly float gravity = 20f;
    public static readonly int maxWaves = 5;
}
