﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Timer
{
    [SerializeField] private float timer = 0;
    [SerializeField] private float originalTimer = 1;
    [SerializeField] public bool isRunning;
    [SerializeField] public bool repeating = true;

    public Timer(float timer = 1, bool repeat = true, bool stop = false)
    {
        isRunning = true;
        this.timer = timer;
        originalTimer = timer;
        this.repeating = repeat;
        if(stop)
        {
            Stop();
        }
    }

    public void TickFrame()
    {
        if(isRunning)
        {
            timer -= Time.deltaTime;
        }
    }

    public bool IsTriggered()
    {
        bool returnVal = false;

        if (timer <= 0f)
        {
            returnVal = true;
            if(!repeating)
            {
                Stop();
            }
            else
            {
                Reset();
            }
        }

        return returnVal;
    }

    public void Pause()
    {
        isRunning = false;
    }

    public void Resume()
    {
        isRunning = true;
    }

    public void Reset()
    {
        timer = originalTimer;
    }

    public void Stop()
    {
        timer = originalTimer;
        isRunning = false;
    }

    public void PreHeat()
    {
        timer = 0f;
    }

    public void SetTime(float time)
    {
        timer = time;
    }
}
