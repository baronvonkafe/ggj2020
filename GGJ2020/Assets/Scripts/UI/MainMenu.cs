﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Audio;

public class MainMenu : MonoBehaviour
{
    public Canvas settingsCanvas;
    public TextMeshProUGUI volumeText;

    private void Start()
    {
        settingsCanvas.GetComponent<Canvas>().enabled = false;
    }

    public void StartGame()
    {
        SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void OpenSettings()
    {
        if(settingsCanvas.GetComponent<Canvas>().isActiveAndEnabled)
        {
            settingsCanvas.GetComponent<Canvas>().enabled = false;
        }
        else
        {
            settingsCanvas.GetComponent<Canvas>().enabled = true;
        }
    }

    public void CloseSettings()
    {
        settingsCanvas.GetComponent<Canvas>().enabled = false;
    }

    public void OnAudioSliderChanged(float volume)
    {
        AudioManager.Instance.audioMixer.SetFloat("Volume", Mathf.Log10(volume) * 20);
    }

    public void UpdateVolumeText(float volume)
    {
        volumeText.text = volume.ToString("F2") + "%";
    }

    public void OnMuteClicked()
    {
        AudioManager.Instance.ToggleMute();
    }

}
