﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainGameUI : Singleton<MainGameUI>
{

    [SerializeField] private GameObject pausePanel;
    [SerializeField] private GameObject loseGamePanel;
    [SerializeField] private GameObject victoryScreen;
    [SerializeField] private GameObject settingsScreen;

    private void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        pausePanel.SetActive(false);
        loseGamePanel.SetActive(false); 
        victoryScreen.SetActive(false);
        settingsScreen.SetActive(false);
    }

    public void PauseGame()
    {
        pausePanel.SetActive(true);
        Time.timeScale = 0f;
    }

    public void UnPauseGame()
    {
        settingsScreen.SetActive(false);
        pausePanel.SetActive(false);
        Time.timeScale = 1f;
    }

    public bool isPauseActive()
    {
        return pausePanel.activeSelf;
    }

    public void showLoseGamePanel()
    {
        loseGamePanel.SetActive(true);
    }

    public void RestartGame()
    {
        GameController.Instance.RestartGame();
    }

    public void ShowVictoryScreen()
    {
        victoryScreen.SetActive(true);
    }

    public void ShowSettings()
    {
        settingsScreen.SetActive(true);
    }

    public void HideSettings()
    {
        settingsScreen.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void OnAudioSliderChanged(float volume)
    {
        AudioManager.Instance.audioMixer.SetFloat("Volume", Mathf.Log10(volume) * 20);
    }

    public void OnMuteClicked()
    {
        AudioManager.Instance.ToggleMute();
    }
}
