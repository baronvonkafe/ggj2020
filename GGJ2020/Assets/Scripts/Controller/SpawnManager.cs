﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using PathCreation;

public enum PathType
{
    easyPath = 0,
    mediumPath = 1,
    hardPath = 2
}
public class SpawnManager : Singleton<SpawnManager>
{
    public GameObject enemyPrefab;
    public GameObject enemyContainer;

    public int enemiesLeft;
    public List<Wave> waves;
    public int currentWave = 0;
    public float spawnTime = 3f;
    public TextMeshProUGUI infoUI;
    bool spawning = false;

    public PathCreator easyPath;
    public PathCreator mediumPath;
    public PathCreator hardPath;

    private void Awake()
    {
        base.Awake();
        spawnTime = 1f;
        enemiesLeft = waves[0].enemyNumber;
    }

    private void Update()
    {
        if(enemiesLeft <= 0 && !spawning)
        {
            spawning = true;
            StartCoroutine(SpawnNextWave());
        }
    }

    IEnumerator SpawnWave()
    {
        UpdateInfoText();
        for (int i = 0; i < waves[currentWave].enemyNumber; i++)
        {
            GameObject elf = Instantiate(enemyPrefab, enemyContainer.transform, true);
            AssignElfModifiers(elf);
            yield return new WaitForSeconds(spawnTime);
        }
        yield return new WaitForEndOfFrame();
        spawning = false;
    }

    IEnumerator SpawnNextWave()
    {
        currentWave++;
        if(currentWave < waves.Count)
        {
            spawnTime -= 0.3f;
            enemiesLeft = waves[currentWave].enemyNumber;
            yield return new WaitForSeconds(1f);
            StartCoroutine(SpawnWave());
        }
        else
        {
            yield return new WaitForSeconds(1f);
            GameController.Instance.WinGame();
        }
    }

    public void StartTheGame()
    {
        if(waves.Count == 0)
        {
            Debug.LogError("Add some waves bro!");
        }
        StartCoroutine(SpawnWave());
    }

    public void KillEnemy()
    {
        enemiesLeft--;
    }

    void UpdateInfoText()
    {
        infoUI.text = waves[currentWave].waveName;
    }

    void AssignElfModifiers(GameObject elf)
    {
        AssignPathToEnemy(elf);
        Enemy currecntEnemy = elf.GetComponent<Enemy>();
        currecntEnemy.stats.damage.AddMultiplier(waves[currentWave].attackModifier);
        currecntEnemy.damageable.maxHealth *= waves[currentWave].healthModifier;
        currecntEnemy.stats.speed.AddMultiplier(waves[currentWave].speedModifier);
    }
    void AssignPathToEnemy(GameObject enemy)
    {
        if(waves[currentWave].randomPaths)
        {
            waves[currentWave].pathType = (PathType)Random.Range(0, 2);
        }

        switch (waves[currentWave].pathType)
        {
            case PathType.easyPath:
                enemy.GetComponent<Enemy>().pathCreator = easyPath;
                break;
            case PathType.mediumPath:
                enemy.GetComponent<Enemy>().pathCreator = mediumPath;
                break;
            case PathType.hardPath:
                enemy.GetComponent<Enemy>().pathCreator = hardPath;
                break;
            default:
                break;
        }
    }
}
