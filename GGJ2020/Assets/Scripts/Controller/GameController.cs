﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : Singleton<GameController>
{
    private void Awake()
    {
        base.Awake();
    }

    public void StartGame()
    {
        AudioManager.Instance.PlayElvenWarhorn();
        SpawnManager.Instance.StartTheGame();
    }

    public void LoseGame()
    {
        // Play Lose game sound
        MainGameUI.Instance.showLoseGamePanel();
    }

    public void WinGame()
    {
        MainGameUI.Instance.ShowVictoryScreen();
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void GoBackToMainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
