﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : Singleton<CameraController>
{
    Transform playerToFollow;
    public Vector3 offset;
    public float currentZoom = 8;
    public float minZoom = 5;
    public float maxZoom = 10;

    private void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        playerToFollow = PlayerController.Instance.gameObject.transform;
        offset = transform.position - playerToFollow.position;
    }

    private void LateUpdate()
    {
        this.transform.position = playerToFollow.position + offset;

    }

    private void ZoomCamera()
    {
        currentZoom -= Input.mouseScrollDelta.y;
        currentZoom = Mathf.Clamp(currentZoom, minZoom, maxZoom);
        GetComponent<Camera>().orthographicSize = currentZoom;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(this.transform.position, transform.position + transform.forward * 20f);
    }
}
