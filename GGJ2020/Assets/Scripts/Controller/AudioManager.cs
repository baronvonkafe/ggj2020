﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : Singleton<AudioManager>
{
    public AudioSource effectAudioSource;
    public AudioSource musicAudioSource;
    public AudioClip currencyCollected;
    public AudioClip warhorn;
    public AudioClip music;
    [SerializeField] private Sound[] sounds;
    public AudioMixer audioMixer;
    float oldVolume;

    private void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(this.gameObject);
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.outputAudioMixerGroup = audioMixer.outputAudioMixerGroup;
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
        }
    }

    private void Start()
    {
        PlayMainTheme();
    }

    public void ApplyAudioMixer(AudioSource source)
    {
        source.outputAudioMixerGroup = audioMixer.outputAudioMixerGroup;
    }

    public void Play(string name)
    {
        Sound s = System.Array.Find(sounds, sound => sound.name == name);
        if(s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        s.source.Play();
    }

    public void Play(AudioClip clip)
    {
        musicAudioSource.clip = clip;
        musicAudioSource.Play();
    }

    public void PlayMainTheme()
    {
        musicAudioSource.clip = music;
        musicAudioSource.Play();
    }

    public void PlayCurrencyCollectSound()
    {
        effectAudioSource.pitch = Random.Range(0.7f, 1.3f);
        effectAudioSource.PlayOneShot(currencyCollected);
    }

    public void PlayElvenWarhorn()
    {
        musicAudioSource.PlayOneShot(warhorn);
    }

    public void PlayOneShot(AudioClip clip)
    {
        effectAudioSource.PlayOneShot(clip);
    }

    public void PlayOneShot(AudioClip clip, float minPitch, float maxPitch)
    {
        effectAudioSource.pitch = Random.Range(minPitch, maxPitch);
        effectAudioSource.PlayOneShot(clip);
    }

    public void ToggleMute()
    {

        AudioListener.pause = !AudioListener.pause;

        //float volume;
        //AudioManager.Instance.audioMixer.GetFloat("Volume", out volume);
        //if (volume > 0)
        //{
        //    oldVolume = volume;
        //    AudioManager.Instance.audioMixer.SetFloat("Volume", 0);
        //}
        //else
        //{
        //    AudioManager.Instance.audioMixer.SetFloat("Volume", volume);
        //}
    }
}
