﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : Singleton<PlayerController>
{
    public PlayerStats stats;
    CharacterController controller;
    public Animator animator;
    public Collider interactionRadius;
    public GameObject turrentParent;
    public List<GameObject> turretPrefabs;
    [SerializeField] GameObject selectedTurret;
    public TextMeshProUGUI currencyText;
    AudioSource audio;
    bool firstRepair = true;

    Vector3 inputDirection;
    Vector3 moveDirection;

    float rotationSpeed = 10;

    private void Awake()
    {
        base.Awake();
        controller = GetComponent<CharacterController>();
        turretPrefabs = new List<GameObject>();
        audio = GetComponent<AudioSource>();
        UpdateCurrency();
    }

    private void Update()
    {
        CheckPlayerInputs();
        MovePlayer();
        LookTowardsMouse();
    }

    void CheckPlayerInputs()
    {
        inputDirection = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;
        //inputDirection = CameraController.Instance.transform.TransformDirection(inputDirection);
        //inputDirection.y = 0;

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(MainGameUI.Instance.isPauseActive())
            {
                MainGameUI.Instance.UnPauseGame();
            }
            else
            {
                MainGameUI.Instance.PauseGame();
            }
        }
        else if(Input.GetKeyDown(KeyCode.F) || (Input.GetMouseButtonDown(0)))
        {
            if(selectedTurret != null && selectedTurret.GetComponent<Turret>().stats.isBroken)
            {
                selectedTurret.GetComponent<Turret>().RepairTurret();
                animator.SetTrigger("Repair");
                audio.Play();
                if(firstRepair)
                {
                    GameController.Instance.StartGame();
                    firstRepair = false;
                }
            }
        }
    }

    void MovePlayer()
    {
        Vector3 fromCameraToMe = transform.position - CameraController.Instance.transform.position;
        fromCameraToMe.y = 0;
        fromCameraToMe.Normalize();

        moveDirection = new Vector3(inputDirection.x * stats.speed.GetValue(), 0, inputDirection.z * stats.speed.GetValue());

        Vector3 animationInput = Camera.main.transform.forward * inputDirection.z + Camera.main.transform.right * inputDirection.x;
        animationInput = transform.InverseTransformDirection(inputDirection);
        
        //Debug.Log(inputDirection);

        animator.SetFloat("InputHorizontal", animationInput.x);
        animator.SetFloat("InputVertical", animationInput.z);

        //Need to figure out camera based movement if we want to turn the camera
        //moveDirection = CameraController.Instance.transform.forward * moveDirection.z + CameraController.Instance.transform.right * moveDirection.x;
        //moveDirection.y -= Constants.gravity * Time.deltaTime;

        controller.Move(moveDirection * Time.deltaTime);
    }

    void LookTowardsMouse()
    {
        Plane plane = new Plane(Vector3.up, transform.position);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        float hitDist = 0.0f;

        if(plane.Raycast(ray, out hitDist))
        {
            Vector3 targetPoint = ray.GetPoint(hitDist);
            Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("RepairArea"))
        {
            selectedTurret = other.transform.parent.gameObject;
        }
        else if(other.gameObject.CompareTag("Currency"))
        {
            stats.currency++;
            UpdateCurrency();
            Destroy(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Turret"))
        {
            if(selectedTurret == other.gameObject)
            {
                selectedTurret = null;
            }
        }
    }

    public void UpdateCurrency()
    {
        currencyText.text = stats.currency.ToString() + " x ";
    }
}
