﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum EnemyType
{
    Elf,
    BoomerElf,
    ChildElf
}

[Serializable]
public class EnemyStats : CharacterStats
{
    public EnemyType enemyType;
    public Damageable damageable;
}
