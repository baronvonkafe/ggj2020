﻿using System;
using UnityEngine;

[Serializable]
public class Damageable
{
    public float maxHealth;
    public float startingHealth;
    public float currentHealth;

	public float normalisedHealth
	{
		get
		{
			if (Math.Abs(maxHealth) <= Mathf.Epsilon)
			{
				Debug.LogError("Max Health is 0");
				maxHealth = 1f;
			}
			return currentHealth / maxHealth;
		}
	}

	public bool isDead
	{
		get { return currentHealth <= 0f; }
	}

	public bool isAtMaxHealth
	{
		get { return Mathf.Approximately(currentHealth, maxHealth); }
	}

	public bool TakeDamage(float damage)
	{
		if(isDead)
		{
			return false;
		}

		ChangeHealth(-damage);

		return true;
	}

	protected void ChangeHealth(float healthChange)
	{
		currentHealth += healthChange;
		currentHealth = Mathf.Clamp(currentHealth, 0f, maxHealth);
	}
}
