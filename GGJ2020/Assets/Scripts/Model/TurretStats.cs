﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class TurretStats
{
    public Stat attackSpeed;
    public Stat range;
    public Stat attack;

    public Stat currentHeat;
    public Stat maxHeat;
    public bool jammed;
    public bool isBroken;

}
