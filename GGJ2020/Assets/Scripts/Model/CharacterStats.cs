﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class CharacterStats
{
    public string name;

    public Stat speed;
    public Stat attackSpeed;
    public int currency;
    public Stat damage;
}
