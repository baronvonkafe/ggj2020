﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Projectile : MonoBehaviour
{
    public GameObject parentTurret;
    public Transform target;
    public float speed;
    public float damage;

    public void Init(GameObject parentTurret, Transform target, float damage)
    {
        this.parentTurret = parentTurret;
        this.target = target;
        this.damage = damage;
    }
    
    private void Update()
    {
        if(target != null)
        {
            FlyToTarget();
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void FlyToTarget()
    {
        transform.LookAt(target.transform);
        if (Vector3.Distance(transform.position, target.position) > 0.001f)
        {
            float step = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, target.position, step);
        }
        else
        {
            transform.position = target.position;
        }
    }
}
