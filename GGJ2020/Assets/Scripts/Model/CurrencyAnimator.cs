﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrencyAnimator : MonoBehaviour
{

    public float rotationSpeed = 30;
    float angle = 0;

    void Update()
    {
        transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);
        transform.Translate(0, Mathf.Sin(angle) / 500f, 0);
        angle += Time.deltaTime;
        if (angle > 360)
        {
            angle -= 360;
        }
    }
}
