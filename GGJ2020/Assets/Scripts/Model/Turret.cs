﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TurretType
{
    Basic,
    Fast,
    Mortar,
    Flame
}

[RequireComponent(typeof(SphereCollider))]
public class Turret : MonoBehaviour
{
    public TurretType type;
    public TurretStats stats;
    public SphereCollider attackRange;
    public Timer repairCooldown;
    private Timer attackCooldown;
    private GameObject rotatingPart;
    public List<GameObject> shootingTarget;
    public GameObject projectilePrefab;
    public Transform projectileSpawnPoint;
    public GameObject turretCanvas;
    public GameObject repairSign;
    public RectTransform heatMeter;
    AudioSource gunshotAudio;

    private void Start()
    {
        attackRange = GetComponent<SphereCollider>();
        attackRange.radius *= stats.range.GetValue();
        rotatingPart = transform.GetChild(0).gameObject;
        shootingTarget = new List<GameObject>();
        repairSign.SetActive(false);
        attackCooldown = new Timer((float)1f / (stats.attackSpeed.GetValue()), true);
        attackCooldown.Stop();
        attackCooldown.PreHeat();
        gunshotAudio = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if(shootingTarget.Count != 0 && !stats.isBroken)
        {
            if(shootingTarget[0] != null)
            {
                LookAtElves();
                ShootTarget();
            }
            else
            {
                if(shootingTarget.Count > 0)
                {

                }
                shootingTarget.Clear();
            }
        }
        CheckIfBroken();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            shootingTarget.Add(other.gameObject);
            attackCooldown.Resume();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            RemoveEnemyFromShootingTargets(other.gameObject);
        }
    }

    public void RemoveEnemyFromShootingTargets(GameObject enemy)
    {
        if (shootingTarget.Contains(enemy))
        {
            shootingTarget.Remove(enemy);

            if (shootingTarget.Count == 0)
            {
                attackCooldown.Stop();
                attackCooldown.PreHeat();
            }
        }
    }

    void ShootTarget()
    {
        attackCooldown.TickFrame();
        if (attackCooldown.IsTriggered())
        {
            GameObject newBullet = Instantiate(projectilePrefab, projectileSpawnPoint, true);
            newBullet.transform.position = projectileSpawnPoint.position;
            newBullet.GetComponent<Projectile>().Init(this.gameObject, shootingTarget[0].GetComponent<Enemy>().bulletTarget, stats.attack.GetValue());
            stats.currentHeat.AddToBaseValue(1);

            if(gunshotAudio.clip != null)
            {
                AudioManager.Instance.PlayOneShot(gunshotAudio.clip);
            }
        }
    }

    void LookAtElves()
    {
        rotatingPart.transform.LookAt(shootingTarget[0].transform);
    }

    void CheckIfBroken()
    {
        if(stats.currentHeat.GetValue() >= stats.maxHeat.GetValue() || stats.isBroken)
        {
            stats.isBroken = true;
            repairSign.SetActive(true);
        }
        UpdateHeatBar();
    }

    public void RepairTurret()
    {
        stats.isBroken = false;
        stats.currentHeat.SetBaseValue(0);
        repairSign.SetActive(false);
    }

    public void UpdateHeatBar()
    {
        float heatPercent = stats.currentHeat.GetValue() / stats.maxHeat.GetValue();
        heatMeter.sizeDelta = new Vector2(heatPercent * 1.9f, heatMeter.sizeDelta.y);
    }
}
