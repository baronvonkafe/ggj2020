﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class Enemy : MonoBehaviour
{
    public PathCreator pathCreator;
    public EnemyStats stats;
    public Damageable damageable;
    public float distanceTravelled;
    Timer attackCooldown;
    public Transform bulletTarget;
    public GameObject canvas;
    public RectTransform hpBar;
    public Animator animator;
    public Transform currencyParent;
    public GameObject currencyPrefab;
    GameObject lastTurretThatShot;
    AudioSource elfDeath;

    public float turnSpeed = 10f;

    private void Start()
    {
        attackCooldown = new Timer((float)1f / (stats.attackSpeed.GetValue()), true);
        elfDeath = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if(pathCreator == null)
        {
            Debug.LogError("Elf " + gameObject.name + " has no PathCreator assigned!");
            return;
        }

        if(IsDestinationReached())
        {
            AttackBase();
        }
        else
        {
            WalkOnPath();
        }

        if(damageable.isDead)
        {
            Die();
        }

        attackCooldown.TickFrame();
        UpdateHealthBar();
    }

    void WalkOnPath()
    {
        distanceTravelled += stats.speed.GetValue() * Time.deltaTime;
        transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled, EndOfPathInstruction.Stop);
        transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled, EndOfPathInstruction.Stop);
    }

    bool IsDestinationReached()
    {
        return (distanceTravelled >= pathCreator.path.length);
    }

    void AttackBase()
    {
        if(attackCooldown.IsTriggered())
        {

            BaseCanoon.Instance.TakeDamage(stats.damage.GetValue());
        }
    }

    void Die()
    {
        animator.SetTrigger("Death");
        attackCooldown.Stop();
        AudioManager.Instance.PlayOneShot(elfDeath.clip, 0.7f, 1.3f);
        // play death animation
        // Drop currency
        GameObject bucket = Instantiate(currencyPrefab, currencyParent, true);
        bucket.transform.position = this.transform.position;
        lastTurretThatShot.GetComponent<Turret>().RemoveEnemyFromShootingTargets(this.gameObject);
        SpawnManager.Instance.KillEnemy();
        this.gameObject.GetComponent<Enemy>().enabled = false;
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Bullet"))
        {
            lastTurretThatShot = other.gameObject.GetComponent<Projectile>().parentTurret;
            damageable.TakeDamage(other.gameObject.GetComponent<Projectile>().damage);
            Destroy(other.gameObject);
        }
    }

    public void UpdateHealthBar()
    {
        float healthPercent = damageable.currentHealth / damageable.maxHealth;
        hpBar.sizeDelta = new Vector2(healthPercent * 0.98f, hpBar.sizeDelta.y);

        if (damageable.currentHealth >= damageable.maxHealth)
        {
            canvas.SetActive(false);
        }
        else
        {
            canvas.SetActive(true);
        }
    }
}
