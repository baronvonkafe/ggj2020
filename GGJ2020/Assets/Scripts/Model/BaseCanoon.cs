﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseCanoon : Singleton<BaseCanoon>
{
    public Damageable damageable;
    public RectTransform hpBar;

    private void Awake()
    {
        base.Awake();
    }

    public void TakeDamage(float damage)
    {
        damageable.TakeDamage(damage);
        UpdateHealthBar();

        if(damageable.isDead)
        {
            // Lose game
            GameController.Instance.LoseGame();
        }
    }

    public void UpdateHealthBar()
    {
        float healthPercent = damageable.currentHealth / damageable.maxHealth;
        hpBar.sizeDelta = new Vector2(healthPercent * 495, hpBar.sizeDelta.y);
    }
}
