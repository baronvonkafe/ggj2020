﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Wave", menuName = "Wave")]
public class Wave : ScriptableObject
{
    public string waveName = "";
    public int enemyNumber = 1;
    public PathType pathType;
    public float speedModifier = 1;
    public float attackModifier = 1;
    public float healthModifier = 1;
    public bool randomPaths = false;

}
