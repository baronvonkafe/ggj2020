﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Stat
{
    [SerializeField]
    private float baseValue = 0;

    private List<float> modifiers = new List<float>();
    private List<float> multipliers = new List<float>();

    public float GetValue()
    {
        float finalValue = baseValue;
        modifiers.ForEach(x => finalValue += x);
        multipliers.ForEach(x => finalValue *= x);
        return finalValue;
    }

    public void AddToBaseValue(float value)
    {
        if (value != 0)
        {
            baseValue += value;
        }
    }

    public float GetBaseValue()
    {
        return baseValue;
    }

    public void SetBaseValue(float value)
    {
        baseValue = value;
    }

    public void AddModifier(float modifier)
    {
        if (modifier != 0)
        {
            modifiers.Add(modifier);
        }
    }

    public void RemoveModifier(float modifier)
    {
        if (modifier != 0)
        {
            modifiers.Remove(modifier);
        }
    }

    public void AddMultiplier(float multiplier)
    {
        if (multiplier != 0)
        {
            multipliers.Add(multiplier);
        }
    }

    public void RemoveMultiplier(float multiplier)
    {
        if (multiplier != 0)
        {
            multipliers.Remove(multiplier);
        }
    }
}
